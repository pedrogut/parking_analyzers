package parking.analyzer.license;


import parking.recognizer.analyzer.AnalysisResult;
import parking.recognizer.analyzer.Analyzer;

import java.awt.*;

public class LowLightAnalyzer implements Analyzer {

    @Override
    public AnalysisResult analyze(Image image) {
        return new AnalysisResult("ABC123", 0.7);
    }

}
