package parking.analyzer.exceptions;

public class AnalyzeCorruptImageException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public AnalyzeCorruptImageException(String file) {
        super(String.format("File %s is corrupted.", file));
    }
}
